# Please find the details of the feature implemented

# Language used:

*  Kotlin

# Key featured used:

*  Coroutines with retrofit
*  Static analysis tool - detekt
*  MVVM
*  Dagger2 (Feature Scope)
*  Data binding
*  Room - Async CRUD operation
*  Unit test
*  Glide with Data binding
*  Debounce listener
*  Observer pattern
*  Customised Toast
*  Boradcast Receiver
*  Shimmer
  

# Gitlab

*  Comitted with proper commit message and merge

# To run static analysis

*  detekt has been integrated with full code pass. Please clean and run ./gradlew evaluateViolations to verify

# What all implemented

*  Cache-First Data
*  Search with default search term
*  From retrofit 2.6.0, it suspends the api call for seamless experience using coroutines
*  Feature level dagger injection using scopes
*  Glidemodule
*  Handling UI state using databinding
*  Generic response state handler
*  Generated GlideApp for easy use of glide apis
*  Implemented debounce listener for typing delay search
*  Objectutils for good code readbility
*  Unit tested the model since model validation is an important layer for api based feature
*  Internet handling


