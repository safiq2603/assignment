package com.saf.work

import com.saf.work.feature.search.core.SearchViewModel
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.runners.MockitoJUnitRunner

/**
 *Unit test for search model - since model is an important layer for any api based feature
 */
@RunWith(MockitoJUnitRunner::class)
class SearchModelTest {

    var mockResponse: MockResponse? = null
    var viewModel: SearchViewModel? = null

    @Before
    fun setup() {
        mockResponse = MockResponse()
        viewModel = SearchViewModel()
    }

    //Failure Scenarios
    @Test
    fun test_null_response() {
        val actual = viewModel?.validateResponse(null)

        Assert.assertFalse(actual!!)
    }

    @Test
    fun test_empty_list_return_false() {
        val actual = viewModel?.validateResponse(mockResponse?.respWithEmptyList)

        Assert.assertFalse(actual!!)
    }

    @Test
    fun test_response_failure_return_false() {
        val actual = viewModel?.validateResponse(mockResponse?.respWithFailure)

        Assert.assertFalse(actual!!)
    }

    @Test
    fun test_empty_status_return_false() {
        val actual = viewModel?.validateResponse(mockResponse?.respWithEmptyStatus)

        Assert.assertFalse(actual!!)
    }

    //Success Scenarios
    @Test
    fun test_valid_response_return_true() {
        val actual = viewModel?.validateResponse(mockResponse?.respWithSuccess)

        assert(actual!!)
    }

    @After
    fun clear() {
        mockResponse = null
    }
}
