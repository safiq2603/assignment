package com.saf.work

import com.saf.work.feature.search.model.SearchModel

/**
For now, building the response using object due to time constraint. But, good approach
is to store on json file and read from it
 */
class MockResponse {

    lateinit var validResponse: SearchModel
    lateinit var respWithEmptyList: SearchModel
    lateinit var respWithFailure: SearchModel
    lateinit var respWithSuccess: SearchModel
    lateinit var respWithEmptyStatus: SearchModel

    init {
        buildValidResponse()

        buildResponseWithEmptyList()
        buildResponseWithEmptyStatus()
        buildResponseWithFailure()
        buildResponseWithSuccess()
    }

    //Success Scenarios
    private fun buildValidResponse() {
        validResponse = SearchModel(mutableListOf(), "True", "10")
    }


    //Error scenarios
    private fun buildResponseWithEmptyList() {
        respWithEmptyList = SearchModel(null, "True", "10")
    }

    private fun buildResponseWithFailure() {
        respWithFailure = SearchModel(mutableListOf(), "False", "10")
    }

    private fun buildResponseWithSuccess() {
        respWithSuccess = SearchModel(mutableListOf(), "True", "10")
    }

    private fun buildResponseWithEmptyStatus() {
        respWithEmptyStatus = SearchModel(mutableListOf(), null, "10")
    }
}