package com.saf.work.master

import javax.inject.Scope

/**
Maintains the dependency through the complete lifecycle using Scopes
Lifecycle - Per application, Per feature
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SearchScope
