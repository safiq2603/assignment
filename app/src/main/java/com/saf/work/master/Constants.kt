package com.saf.work.master

object Constants {
    const val DB_NAME = "movie_db"
    const val DEFAULT_SEARCH_TERM = "friends"
    const val BASE_URL = "http://www.omdbapi.com/"
    const val API_KEY = "4ef61d7"
    const val TIMEOUT: Long = 30
}
