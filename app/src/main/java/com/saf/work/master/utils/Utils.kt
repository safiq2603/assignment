package com.saf.work.master.utils

import android.content.Context
import android.net.ConnectivityManager

object Utils {
    fun hasDataConnectivity(context: Context): Boolean {
        val connManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (null != connManager) {
            val activeNetwork = connManager.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }
        return false
    }
}
