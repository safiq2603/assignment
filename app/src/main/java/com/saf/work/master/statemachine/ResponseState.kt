package com.saf.work.master.statemachine

/**
State handler for all network response with success/error/loading state
 */
data class ResponseState<out T>(
    val status: Status,
    val data: T?,
    val msg: String?
) {
    companion object {
        fun <T> success(data: T?): ResponseState<T> {
            return ResponseState(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(msg: String, data: T? = null): ResponseState<T> {
            return ResponseState(
                Status.ERROR,
                data,
                msg
            )
        }

        fun <T> loading(data: T? = null): ResponseState<T> {
            return ResponseState(
                Status.LOADING,
                data,
                null
            )
        }
    }
}

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
