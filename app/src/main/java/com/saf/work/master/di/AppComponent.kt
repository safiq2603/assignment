package com.saf.work.master.di

import android.app.Application
import com.saf.work.master.ApplicationScope
import com.saf.work.core.room.AppDatabase
import dagger.Component
import okhttp3.OkHttpClient
import retrofit2.Retrofit

@ApplicationScope
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(app: Application)

    fun provideDatabase(): AppDatabase
    fun provideRetrofit(): Retrofit
    fun provideOkHttpClient(): OkHttpClient
}
