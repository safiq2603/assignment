package com.saf.work.master.utils

import android.text.TextUtils

/*
Utils to check for null or empty operation on any objects for good code readability
 */
object ObjectUtils {
    fun notEmpty(obj: Any?): Boolean {
        return null != obj
    }

    fun notEmpty(array: Array<Any>): Boolean {
        return array != null && array.isNotEmpty()
    }

    fun notEmpty(list: List<*>): Boolean {
        return list != null && list.isNotEmpty()
    }

    fun notEmpty(map: Map<*, *>?): Boolean {
        return map != null && map.isNotEmpty()
    }

    fun notEmpty(string: String): Boolean {
        return !TextUtils.isEmpty(string)
    }

    fun isEmpty(string: String): Boolean {
        return TextUtils.isEmpty(string)
    }
}
