package com.saf.work.master.ui

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.saf.work.R
import com.saf.work.core.reciever.DataConnectivityChangesReceiver
import com.saf.work.core.reciever.Event
import com.saf.work.feature.search.ui.SearchFragment
import com.saf.work.master.utils.Utils.hasDataConnectivity
import com.saf.work.master.utils.ViewUtils.showToast

class MainActivity : AppCompatActivity() {

    private lateinit var connObserver: DataConnectivityChangesReceiver
    var connectivityLive: MutableLiveData<Event<Boolean>>

    init {
        connectivityLive = MutableLiveData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addSearchFragment()
    }

    /*
    transact to add Searchfragment
     */
    private fun addSearchFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, SearchFragment.newInstance())
            .commitNow()
    }

    override fun onStart() {
        super.onStart()

        registerReceiver()
        observe()
    }

    /*
    Observes for connectivity change
     */
    private fun observe() {
        connectivityLive.observe(this, Observer {
            if (it.peekContent()!!) {
                showToast(
                    this,
                    resources.getString(R.string.online),
                    resources.getColor(R.color.color_white),
                    R.drawable.layout_toast_background
                )
            } else {
                showToast(
                    this,
                    resources.getString(R.string.no_internet),
                    resources.getColor(R.color.color_white),
                    R.drawable.layout_toast_background_offline
                )
            }
        })
    }

    /*
    Register for connectivity broadcasting receiver
     */
    private fun registerReceiver() {
        connObserver = DataConnectivityChangesReceiver(
            hasDataConnectivity(this),
            connectivityLive
        )
        registerReceiver(
            connObserver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    /*
    Unregisters for connectivity broadcasting receiver
     */
    override fun onStop() {
        super.onStop()

        unregisterReceiver(connObserver)
    }
}
