package com.saf.work.master.utils

import android.content.Context
import android.widget.TextView
import android.widget.Toast

object ViewUtils {
    fun showToast(context: Context, text: String, colorId: Int, drawableId: Int) {
        val toast = Toast.makeText(context, text, Toast.LENGTH_LONG)
        val toastView = toast.view

        val tv = toastView.findViewById(android.R.id.message) as TextView

        tv.setTextColor(colorId)
        toastView.setBackgroundResource(drawableId)
        toast.show()
    }
}
