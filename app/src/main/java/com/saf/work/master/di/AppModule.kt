package com.saf.work.master.di

import android.app.Application
import androidx.room.Room
import com.saf.work.core.room.AppDatabase
import com.saf.work.master.ApplicationScope
import com.saf.work.master.Constants
import com.saf.work.master.Constants.TIMEOUT
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


@Module
class AppModule(var app: Application) {

    @Provides
    @ApplicationScope
    fun provideDatabase(): AppDatabase =
        Room.databaseBuilder(
            app,
            AppDatabase::class.java!!,
            Constants.DB_NAME
        ).allowMainThreadQueries().build()

    @Provides
    @ApplicationScope
    fun provideOkHttpClient(): OkHttpClient {
        var builder: OkHttpClient.Builder = OkHttpClient.Builder()

        builder.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        builder.readTimeout(TIMEOUT, TimeUnit.SECONDS)
        builder.writeTimeout(TIMEOUT, TimeUnit.SECONDS)

        return builder.build()
    }

    @Provides
    @ApplicationScope
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}
