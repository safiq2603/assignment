package com.saf.work.master.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.saf.work.MyApplication
import com.saf.work.master.di.AppComponent

/*
BaseFragment which must be compulsorily extended by other fragments to achieved Dependency injection
 */
abstract class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject(appComponent)
    }

    val appComponent: AppComponent
        get() = (requireActivity().application as MyApplication).appComponent

    /*
    Mandatory injection by all child fragments
     */
    abstract fun inject(appComponent: AppComponent)
}
