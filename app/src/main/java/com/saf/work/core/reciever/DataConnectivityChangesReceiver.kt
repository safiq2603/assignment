package com.saf.work.core.reciever

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import com.saf.work.master.utils.Utils

/*
Connectivity reciever which broadcast when the internet is up/down
 */
class DataConnectivityChangesReceiver(
    var state: Boolean = false,
    val connectivity: MutableLiveData<Event<Boolean>>? = null
) : BroadcastReceiver() {

    init {
        connectivity?.postValue(Event(state))
    }

    override fun onReceive(context: Context, intent: Intent) {
        val newState = Utils.hasDataConnectivity(context)
        if (state != newState) {
            state = newState
            connectivity?.postValue(Event(state))
        }
    }
}
