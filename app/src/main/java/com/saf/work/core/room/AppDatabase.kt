package com.saf.work.core.room

import androidx.room.Database
import androidx.room.RoomDatabase

/*
Represents the physical database isolated per application
 */
@Database(entities = [MovieData::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}
