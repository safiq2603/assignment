package com.saf.work.core.reciever

import androidx.annotation.Nullable

/*
Workaround for (T)livedata that emits same data multiple time
 */
class Event<T>(
    @param:Nullable @field:Nullable
    private val content: T?
) {
    private var hasBeenHandled = false

    /**
     * Returns the content and prevents its use again.
     */
    val contentIfNotHandled: T?
        @Nullable
        get() {
            if (hasBeenHandled) {
                return null
            } else {
                hasBeenHandled = true
                return content
            }
        }

    /**
     * Returns the content, even if it's already been handled.
     */
    @Nullable
    fun peekContent(): T? {
        return content
    }
}
