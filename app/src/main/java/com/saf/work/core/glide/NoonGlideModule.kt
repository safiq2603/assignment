package com.saf.work.core.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/*
Glide wrapper for this application for easy accessible of glide api
 */
@GlideModule
class NoonGlideModule : AppGlideModule()
