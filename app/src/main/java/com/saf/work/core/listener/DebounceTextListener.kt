package com.saf.work.core.listener

import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.*

/*
Lifecycle aware debounce listener which cancel the coroutine job once the view goes to destroy state
 */
class DebounceTextListener(
    lifecycle: Lifecycle,
    private val onDebounceTextChange: (String?) -> Unit
) : SearchView.OnQueryTextListener, LifecycleObserver {

    companion object {
        private const val DEBOUNCE_PERIOD: Long = 500
    }

    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

    private var searchJob: Job? = null

    init {
        lifecycle.addObserver(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    /*
    Delays the job by specified interval without blocking the UI thread
     */
    override fun onQueryTextChange(newText: String): Boolean {
        searchJob?.cancel()
        searchJob = coroutineScope.launch {
            newText?.let {
                delay(DEBOUNCE_PERIOD)
                onDebounceTextChange(newText)
            }
        }
        return false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun destroy() {
        searchJob?.cancel()
    }
}
