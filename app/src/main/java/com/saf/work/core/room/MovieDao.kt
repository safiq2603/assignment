package com.saf.work.core.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/*
Data access object layer which can perform CRUD operation on Room DB
 */
@Dao
interface MovieDao {

    @Query("SELECT * FROM MovieData WHERE search_term=:searchTerm")
    fun getMovieData(searchTerm: String): MovieData

    @Insert
    fun insert(movieData: MovieData)
}
