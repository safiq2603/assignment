package com.saf.work.core.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

/*
Physical entity which represents the table in the DB
 */
@Entity
data class MovieData(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "search_term") val searchTerm: String,
    @ColumnInfo(name = "response") val response: String
) : Serializable
