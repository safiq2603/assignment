package com.saf.work.feature.search.core

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.google.gson.Gson
import com.saf.work.core.room.MovieData
import com.saf.work.feature.search.model.SearchItem
import com.saf.work.feature.search.model.SearchModel
import com.saf.work.master.statemachine.ResponseState
import com.saf.work.master.statemachine.Status
import com.saf.work.master.utils.ObjectUtils.notEmpty
import com.saf.work.master.utils.Utils
import kotlinx.coroutines.Dispatchers

class SearchViewModel(val context: Context? = null, private val repo: SearchRepository? = null) :
    ViewModel() {

    var showError: ObservableField<Boolean>
    var showList: ObservableField<Boolean>
    var showLoader: ObservableField<Boolean>

    var movieList: MutableLiveData<List<SearchItem>>

    init {
        movieList = MutableLiveData()
        showError = ObservableField()
        showList = ObservableField()
        showLoader = ObservableField()
    }

    /*
    Cache-First : Checks if data is cache and returns it and parallely makes api to refresh data
     */
    fun getMoviesWithCache(searchTerm: String) {
        val cacheData: MovieData? = repo?.getMovie(searchTerm)

        if (notEmpty(cacheData)) {
            val movieData: SearchModel =
                Gson().fromJson(cacheData?.response, SearchModel::class.java)
            if (notEmpty(movieData)) {
                movieList.value = movieData.results
            }
            fetchMovie(true, searchTerm)
        } else {
            fetchMovie(false, searchTerm)
        }
    }

    /*
    Starts observing on search movie call
     */
    private fun fetchMovie(cacheAvailable: Boolean, searchTerm: String) {
        if (Utils.hasDataConnectivity(context!!)) {
            makeSearchCall(searchTerm).observeForever { responseState ->
                when (responseState.status) {
                    Status.LOADING -> {
                        showLoader()
                    }
                    Status.SUCCESS -> {
                        showList()
                        movieList.value = responseState.data?.results
                    }
                    Status.ERROR -> {
                        showError()
                    }
                }
            }
        } else if (!cacheAvailable) {
            movieList.value = null
        }
    }

    fun validateResponse(data: SearchModel?): Boolean {
        return (notEmpty(data) && notEmpty(data?.response) && data?.response!!.toBoolean()
                && notEmpty(data?.results))
    }

    /*
    Makes search movie call using coroutines for seamless experience in app
     */
    private fun makeSearchCall(searchTerm: String) = liveData(Dispatchers.IO) {
        emit(ResponseState.loading())
        val response = repo?.fetchMovies(searchTerm)

        if (validateResponse(response?.body())) {
            Log.v("safiq", "succ")
            repo?.insertMovie(
                MovieData(
                    searchTerm = searchTerm,
                    response = Gson().toJson(response?.body())
                )
            )
            emit(ResponseState.success(response?.body()))
        } else {
            emit(ResponseState.error("failure", response?.body()))
        }
    }

    /*
    Factory builder for this viewmodel
     */
    class Factory(val context: Context, val repo: SearchRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return SearchViewModel(context, repo) as T
        }
    }

    /*
    Shows the loader using databinding
     */
    fun showLoader() {
        showError.set(false)
        showList.set(false)
        showLoader.set(true)
    }

    /*
    Shows the error state using databinding
     */
    fun showError() {


        showError.set(true)
        showList.set(false)
        showLoader.set(false)
    }

    /*
    Shows the movie list using databinding
     */
    fun showList() {
        showError.set(false)
        showList.set(true)
        showLoader.set(false)
    }
}
