package com.saf.work.feature.search.core

import android.os.AsyncTask
import com.saf.work.core.room.AppDatabase
import com.saf.work.core.room.MovieData
import com.saf.work.feature.search.api.SearchApi

class SearchRepository(val api: SearchApi, val db: AppDatabase) {

    /*
    From retrofit 2.6.0, it will suspend the api it is firing for seamless experience
    Suspends the api call in light weight thread
     */
    suspend fun fetchMovies(searchTerm: String) = api.fetchMovies(searchTerm)

    /*
    Performs (C)RUD operation to create movie data in DB
     */
    fun insertMovie(movieData: MovieData) {
        object : AsyncTask<Void, Void, Void>() {
            override fun doInBackground(vararg voids: Void): Void? {
                db.movieDao().insert(movieData)
                return null
            }
        }.execute()
    }

    /*
    Performs C(R)UD operation to get movie data from DB
     */
    fun getMovie(searchTerm: String): MovieData {
        return db.movieDao().getMovieData(searchTerm)
    }
}
