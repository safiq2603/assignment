package com.saf.work.feature.search.model

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.google.gson.annotations.SerializedName
import com.saf.work.R
import com.saf.work.core.glide.GlideApp

data class SearchModel(
    @SerializedName("Search") val results: List<SearchItem>?,
    @SerializedName("Response") val response: String?,
    @SerializedName("totalResults") val totalResult: String
)

data class SearchItem(
    @SerializedName("Title") val title: String,
    @SerializedName("Year") val year: String,
    @SerializedName("imdbID") val id: String,
    @SerializedName("Type") val type: String,
    @SerializedName("Poster") val imageUrl: String
) {
    companion object {
        /**
         * Achieves the databinding for glide
         */
        @JvmStatic
        @BindingAdapter("glideUrl")
        fun loadImage(view: AppCompatImageView, imageUrl: String) {
            GlideApp.with(view.context)
                .load(imageUrl)
                .placeholder(R.drawable.noon)
                .into(view)
        }
    }
}
