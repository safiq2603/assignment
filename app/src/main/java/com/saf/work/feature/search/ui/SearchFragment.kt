package com.saf.work.feature.search.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.saf.work.R
import com.saf.work.core.listener.DebounceTextListener
import com.saf.work.databinding.FragmentSearchBinding
import com.saf.work.feature.search.core.SearchViewModel
import com.saf.work.feature.search.di.DaggerSearchComponent
import com.saf.work.feature.search.di.SearchModule
import com.saf.work.feature.search.model.SearchItem
import com.saf.work.master.Constants
import com.saf.work.master.base.BaseFragment
import com.saf.work.master.di.AppComponent
import com.saf.work.master.utils.ObjectUtils.notEmpty
import kotlinx.android.synthetic.main.fragment_search.*
import javax.inject.Inject


class SearchFragment : BaseFragment() {

    @Inject
    lateinit var vm: SearchViewModel

    lateinit var movieListAdapter: MovieListAdapter

    lateinit var binding: FragmentSearchBinding

    companion object {
        @JvmStatic
        fun newInstance() = SearchFragment()
    }

    /*
    Injects this view for databinding
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            inflate(inflater, R.layout.fragment_search, container, false)
        binding.viewModel = vm

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        focusSearchView()
        registerListeners()

        initRecyclerView()

        triggerMovieCall(Constants.DEFAULT_SEARCH_TERM)
    }

    override fun onStart() {
        super.onStart()
        observe()
    }

    /*
    Injects all the dependencies needed for search feature.
    All the dependencies needed for search feature will scale up and down per search lifecycle
     */
    override fun inject(appComponent: AppComponent) {
        DaggerSearchComponent.builder()
            .appComponent(appComponent)
            .searchModule(SearchModule(this))
            .build()
            .inject(this)
    }

    /*
    Registers all the view listeners
     */
    private fun registerListeners() {
        search_view.setOnQueryTextListener(
            DebounceTextListener(
                this.lifecycle
            ) { newText ->
                newText?.let {
                    if (notEmpty(it)) {
                        vm.getMoviesWithCache(it)
                    }
                }
            }
        )
    }

    /*
    Inits the recycler view and inject the databinding to the recycler view item
     */
    private fun initRecyclerView() {
        movieListAdapter = MovieListAdapter()

        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = RecyclerView.VERTICAL
        binding.movieRv.layoutManager = layoutManager

        binding.movieRv.adapter = movieListAdapter
    }

    /*
    updates the data for the movie recycler view and notify on dataset changed
     */
    private fun populateMovies(movieList: List<SearchItem>) {
        movieListAdapter.setData(movieList)
    }

    private fun showLoader() {
        vm.showLoader()
        shimmer_view.startShimmer()
    }

    /*
    Gets focus for search view
     */
    private fun focusSearchView() {
        search_view.onActionViewExpanded()
        search_view.isIconified = false
        search_view.queryHint = resources.getString(R.string.hint)
    }

    /*
    Holds all the observer for this fragment
     */
    private fun observe() {
        vm.movieList.observe(this, Observer
        {
            if (it == null) {
                vm.showError()
            } else {
                populateMovies(it)
                vm.showList()
            }
        })
    }

    override fun onStop() {
        super.onStop()
        vm.movieList.removeObserver { }
    }

    private fun triggerMovieCall(searchTerm: String) {
        vm.getMoviesWithCache(searchTerm)
    }
}
