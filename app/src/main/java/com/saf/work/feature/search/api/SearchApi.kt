package com.saf.work.feature.search.api

import com.saf.work.feature.search.model.SearchModel
import com.saf.work.master.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("/")
    suspend fun fetchMovies(
        @Query("s") searchTem: String,
        @Query("apikey") apiKey: String = Constants.API_KEY
    ): Response<SearchModel>
}
