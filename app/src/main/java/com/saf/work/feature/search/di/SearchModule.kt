package com.saf.work.feature.search.di

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.saf.work.feature.search.api.SearchApi
import com.saf.work.feature.search.core.SearchRepository
import com.saf.work.feature.search.core.SearchViewModel
import com.saf.work.master.SearchScope
import com.saf.work.core.room.AppDatabase
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class SearchModule(var fragment: Fragment) {

    @SearchScope
    @Provides
    fun provideApi(retrofit: Retrofit): SearchApi = retrofit.create(
        SearchApi::class.java)

    @SearchScope
    @Provides
    fun provideRepository(api: SearchApi, db: AppDatabase): SearchRepository =
        SearchRepository(api, db)

    @SearchScope
    @Provides
    fun provideFactory(repo: SearchRepository): SearchViewModel.Factory =
        SearchViewModel.Factory(fragment.context!!, repo)

    @SearchScope
    @Provides
    fun provideViewModel(factory: SearchViewModel.Factory): SearchViewModel =
        ViewModelProviders.of(fragment, factory).get(SearchViewModel::class.java)
}
