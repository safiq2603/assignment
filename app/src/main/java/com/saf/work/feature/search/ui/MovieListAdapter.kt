package com.saf.work.feature.search.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.saf.work.databinding.LayoutMovieItemBinding
import com.saf.work.feature.search.model.SearchItem

class MovieListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var movieList: List<SearchItem>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = LayoutMovieItemBinding.inflate(layoutInflater, parent, false)

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = movieList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(movieList[position])
    }

    fun setData(results: List<SearchItem>) {
        movieList = results
        notifyDataSetChanged()
    }

    inner class ViewHolder(private var applicationBinding: LayoutMovieItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {
        fun bind(item: SearchItem) {
            applicationBinding.movieItem = item
        }
    }
}
