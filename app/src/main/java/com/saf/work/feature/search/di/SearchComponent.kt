package com.saf.work.feature.search.di

import com.saf.work.feature.search.ui.SearchFragment
import com.saf.work.master.SearchScope
import com.saf.work.master.di.AppComponent
import com.saf.work.core.room.AppDatabase
import dagger.Component
import okhttp3.OkHttpClient
import retrofit2.Retrofit

@SearchScope
@Component(modules = [SearchModule::class], dependencies = [AppComponent::class])
interface SearchComponent {

    fun inject(fragment: SearchFragment)

    fun provideDatabase(): AppDatabase
    fun provideRetrofit(): Retrofit
    fun provideOkHttpClient(): OkHttpClient
}
