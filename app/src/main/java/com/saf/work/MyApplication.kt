package com.saf.work

import android.app.Application
import com.saf.work.master.di.AppComponent
import com.saf.work.master.di.AppModule
import com.saf.work.master.di.DaggerAppComponent

class MyApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        inject()
    }

    /*
    Inject dependencies which is needed till application lifecycle
     */
    private fun inject() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}
